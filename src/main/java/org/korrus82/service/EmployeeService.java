package org.korrus82.service;

import org.korrus82.domain.Employee;
import org.korrus82.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Collection;

@Service
@Transactional
public class EmployeeService {

    @Autowired
    protected EmployeeRepository employeeRepository;

    public Employee saveEmployee(Employee emp) {
        return employeeRepository.save(emp);
    }

    public Boolean deleteEmployee(Long empId) {
        Employee temp = employeeRepository.findOne(empId);
        if (temp != null) {
            employeeRepository.delete(temp);
            return true;
        }
        return false;
    }

    public Employee editEmployee(Employee emp) {
        return employeeRepository.save(emp);
    }

    public Collection<Employee> getAllEmployees() {
        Iterable<Employee> itr = employeeRepository.findAll();
        return (Collection<Employee>) itr;
    }

    public Employee findEmployee(Long empId) {
        return employeeRepository.findOne(empId);
    }


}
