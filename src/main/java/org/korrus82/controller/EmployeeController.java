package org.korrus82.controller;

import org.korrus82.domain.Employee;
import org.korrus82.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@ComponentScan
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @RequestMapping(value = {"/", "/savepage"}, method = RequestMethod.GET)
    public String savePage(Model model) {
        model.addAttribute("employee", new Employee());
        model.addAttribute("allEmployees", employeeService.getAllEmployees());
        return "index";
    }

    @RequestMapping(value = {"/employee/save"}, method = RequestMethod.POST)
    public String saveEmployee(@ModelAttribute("employee") Employee employee,
                               final RedirectAttributes redirectAttributes) {
        try {
            if (employeeService.saveEmployee(employee) != null) {
                redirectAttributes.addFlashAttribute("saveEmployee", "success");
            } else {
                redirectAttributes.addFlashAttribute("saveEmployee", "unsuccess");
            }
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute("saveEmployee", "unsuccess");
        }
        return "redirect:/savepage";
    }

    @RequestMapping(value = "/employee/{operation}/{empId}", method = RequestMethod.GET)
    public String editRemoveEmployee(@PathVariable("operation") String operation,
                                     @PathVariable("empId") Long empId, final RedirectAttributes redirectAttributes,
                                     Model model) {

        if (operation.equals("delete")) {
            try {
                if (employeeService.deleteEmployee(empId)) {
                    redirectAttributes.addFlashAttribute("deletion", "success");
                } else {
                    redirectAttributes.addFlashAttribute("deletion", "unsuccess");
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("deletion", "unsuccess");
            }
        } else if (operation.equals("edit")) {
            try {
                Employee editEmployee = employeeService.findEmployee(empId);
                if (editEmployee != null) {
                    model.addAttribute("editEmployee", editEmployee);
                    return "editPage";
                } else {
                    redirectAttributes.addFlashAttribute("status", "notfound");
                }
            } catch (Exception ex) {
                redirectAttributes.addFlashAttribute("status", "unsuccess");
            }
        }

        return "redirect:/savepage";
    }

    @RequestMapping(value = "/employee/update", method = RequestMethod.POST)
    public String updateEmployee(@ModelAttribute("editEmployee") Employee editEmployee,
                                 final RedirectAttributes redirectAttributes) {
        try {
            if (employeeService.editEmployee(editEmployee) != null) {
                redirectAttributes.addFlashAttribute("edit", "success");
            } else {
                redirectAttributes.addFlashAttribute("edit", "unsuccess");
            }
        } catch (Exception ex) {
            redirectAttributes.addFlashAttribute("edit", "unsuccess");
        }
        return "redirect:/savepage";
    }
}